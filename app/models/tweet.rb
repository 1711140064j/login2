class Tweet < ApplicationRecord
  validates :message, presence: true, length: {maximum: 140}

  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :like_users, through: :likes, source: :user
  
  def like(user)
    likes.create(user_id: user.id)
  end
  
  def unlike(user)
    likes.find_by(user_id: user.id).destroy
  end
  
  def liked?(user)
    like?users.include?(user)
  end
  
  def create
    tweet = Tweet.find(params[:tweet_id])
    unless twet.liked?(current_user)
     tweet.like(current_user)
    end
  redirect_to root_path
  end
  
  def destroy
    tweet = Tweet.find(params[:id])
    if tweet.liked?(current_user)
      tweet.unlike(current_user)
    end
    redirect_to root_path
  end
end